import sys
import os
from PyQt5.QtMultimedia import QSound
from graphical_elements import *
from data_spawner import DataSpawner
import os
bundle_dir = getattr(sys, '_MEIPASS', os.path.abspath(os.path.dirname(__file__)))
path_to_sound = os.path.abspath(os.path.join(bundle_dir, 'win.wav'))


class MonoScene(QGraphicsScene):
    def __init__(self):
        super().__init__()
        self.setSceneRect(QRectF(0, 0, 1100, 600))
        brush = QBrush(Qt.black)
        self.setBackgroundBrush(brush)
        self.transform = QTransform()
        self.node_dragged = None  # the node currently being dragged
        self.closest_link = None
        self.tree_pair = None

        self.second_tree_items = set()  # to remove when the tree is updated
        self.possible_links = set()

    def receive_data(self, tree_pair):
        self.clear()
        self.tree_pair = tree_pair
        for node in self.tree_pair.tree1:
            self.add_node(node, False)
        for node in self.tree_pair.tree2:
            self.add_node(node, True)

    def add_node(self, node, is_second_tree):
        if node.is_fantom:
            return
        if node.link:
            self.addItem(node.link)
            if is_second_tree:
                self.second_tree_items.add(node.link)
                self.possible_links.add(node.link)
        self.addItem(node)
        if is_second_tree:
            self.second_tree_items.add(node)

    def remove_highlight(self):
        for item in self.possible_links:
            if item.highlight_mid:
                item.highlight_mid = False

    def flipped(self):
        self.receive_data(self.tree_pair)
        self.check_level_finished()

    def increment_moves(self):
        self.views()[0].parent.increment_moves()

    def mousePressEvent(self, event):
        super().mousePressEvent(event)
        target_item = self.itemAt(event.scenePos(), self.transform)
        if isinstance(target_item, Box):
            target_item = target_item.parentItem()
        if isinstance(target_item, Node):
            self.selection_changed()
            if target_item.movable:
                self.node_dragged = target_item
                self.tree_pair.start_dragging(target_item)

    def mouseMoveEvent(self, event):
        super().mouseReleaseEvent(event)
        if self.node_dragged:
            if self.closest_link:
                self.closest_link[1].highlight_mid = False
            closest_distance, best_candidate = float('Inf'), None
            for c in self.possible_links:
                # filter out the dangling edges
                if c.is_dangling:
                    continue
                # filter out the edges in the subtree
                if c.tail in self.node_dragged.subtree_node_set():
                    continue
                try:
                    distance = abs(event.scenePos().x()-c.midpoint().x())+abs(event.scenePos().y()-c.midpoint().y())
                except RuntimeError:
                    distance = float('Inf')
                if distance < closest_distance:
                    closest_distance = distance
                    best_candidate = c
            self.closest_link = (closest_distance, best_candidate)
            best_candidate.highlight_mid = True

    def mouseReleaseEvent(self, event):
        super().mouseReleaseEvent(event)
        if self.node_dragged:
            move_finished = self.tree_pair.end_dragging(self.node_dragged, self.closest_link)
            self.node_dragged = None
            if move_finished:
                self.receive_data(self.tree_pair)
                self.check_level_finished()
            else:
                self.remove_highlight()
        self.closest_link = None

    def selection_changed(self):
        view = self.views()[0]
        selected = self.selectedItems()
        if not selected:
            view.deselect_node()
            return
        item = selected[0]
        if isinstance(item, Node):
            view.select_node(item)
        else:
            view.deselect_node()

    def check_level_finished(self):
        if self.tree_pair.level_finished():
            self.views()[0].parent.finish_level()


class MonoView(QGraphicsView):
    def __init__(self, parent):
        super().__init__(MonoScene())
        self.parent = parent
        self.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        self.setAcceptDrops(True)
        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)

        self.current_node = None
        self.centerOn(QPoint(0, 0))

    def dropEvent(self, event):
        event.accept()

    def dragEnterEvent(self, event):
        event.accept()

    def dragMoveEvent(self, event):
        event.accept()

    def select_node(self, node):
        self.current_node = node
        self.parent.update_color_hint()

    def deselect_node(self):
        self.current_node = None
        self.parent.clear_color_hint()


class MonoPanel(QWidget):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.view = MonoView(self)
        self.spawner = DataSpawner()
        self.current_level = 'simple1'
        self.current_distance = 0
        self.current_nb_moves = 0
        self.tune = QSound(path_to_sound)

        self.toolbar = QToolBar()
        self.color_box = QComboBox()
        for name in ['viridis', 'plasma', 'inferno', 'magma', 'hot', 'cool', 'spring', 'winter']:
            self.color_box.addItem(name)
        self.color_box.setCurrentIndex(0)
        self.node_color_hint = QLineEdit()
        self.moves_used = QLineEdit()
        self.init_toolbar()
        self.color_box.currentIndexChanged.connect(self.change_color_theme)

        layout = QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.view)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        self.spawn(restart=True)

    def spawn(self, restart):
        self.current_nb_moves = 0
        self.moves_used.setText(str(self.current_nb_moves))
        level, distance, tree_pair = self.spawner.spawn(self.color_box.currentText(),
                                                        self.current_level, restart)
        self.current_level = level
        self.current_distance = distance
        self.view.scene().receive_data(tree_pair)

    def init_toolbar(self):
        label = QLabel('     Moves used  ')
        self.toolbar.addWidget(label)
        self.toolbar.addWidget(self.moves_used)
        for wg in (label, self.moves_used):
            wg.setToolTip('The number of subtree prune-and-regraft operations.')
        label2 = QLabel('     Color theme   ')
        self.toolbar.addWidget(label2)
        self.toolbar.addWidget(self.color_box)
        for wg in (label2, self.color_box):
            wg.setToolTip('Change the color map of the leaves.')
        label3 = QLabel('     Selected color  ')
        self.toolbar.addWidget(label3)
        self.toolbar.addWidget(self.node_color_hint)
        for wg in (label3, self.node_color_hint):
            wg.setToolTip('Select a leaf node to see its color code.')

        self.node_color_hint.setFixedWidth(100)
        self.node_color_hint.setReadOnly(True)
        self.moves_used.setFixedWidth(50)
        self.moves_used.setReadOnly(True)
        self.moves_used.setText(str(self.current_nb_moves))
        self.toolbar.addSeparator()
        self.hint_button = QPushButton('   Show answer   ')
        self.hint_button.setToolTip('Reveal the <b>minimum</b> number of moves.')
        self.hint_button.setFixedWidth(100)
        self.hint_button.clicked.connect(self.hint_dialog)
        spacer = QWidget()
        spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.toolbar.addWidget(spacer)
        self.toolbar.addWidget(self.hint_button)

    def change_color_theme(self, index):
        self.view.scene().tree_pair.change_color_theme(self.color_box.itemText(index))

    def update_color_hint(self):
        # show the color
        current_node = self.view.current_node
        if not current_node:
            return
        if current_node.is_leaf():
            self.node_color_hint.setText(self.view.current_node.color.name())
        else:
            self.node_color_hint.clear()

    def clear_color_hint(self):
        self.node_color_hint.clear()

    def increment_moves(self):
        self.current_nb_moves += 1
        self.moves_used.setText(str(self.current_nb_moves))

    def hint_dialog(self):
        QMessageBox.information(None, 'The optimum',
                                f'The minimum number of moves is {self.current_distance}.',
                                QMessageBox.Ok, QMessageBox.Ok)

    def finish_level(self):
        delay(1)
        self.tune.play()
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle('Level complete')
        msg.setText(f'Congratulations!\n\nYou finished this level using {self.current_nb_moves} '
                    f'move{"" if self.current_nb_moves == 1 else "s"}.\n'
                    f'The minimum number is {self.current_distance}.\n\n')
        msg.addButton('Stay in this configuration', QMessageBox.RejectRole)
        msg.addButton('New level', QMessageBox.YesRole)
        msg.addButton('Restart level', QMessageBox.AcceptRole)
        spacer = QSpacerItem(450, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)
        layout = msg.layout()
        layout.addItem(spacer, layout.rowCount(), 0, 1, layout.columnCount())

        reply = msg.exec()
        if reply == 1: # new level
            self.spawn(False)
        elif reply == 2:  # restart
            self.spawn(True)


class HelloWorld(QWidget):
    def __init__(self):
        super().__init__()
        self.panel = MonoPanel(self)
        help_button = QPushButton('   Help   ', self, icon=self.style().standardIcon(QStyle.SP_DialogHelpButton))
        help_button.setToolTip('Get <b>help</b> on the game controls.')
        restart_button = QPushButton('Restart level')
        new_button = QPushButton('New level')
        for bt in [help_button, restart_button, new_button]:
            bt.setFixedSize(100, 50)

        help_button.clicked.connect(self.help_dialog)
        restart_button.clicked.connect(lambda: self.panel.spawn(True))
        new_button.clicked.connect(lambda: self.panel.spawn(False))

        hlayout = QHBoxLayout()
        hlayout.addWidget(self.panel)
        vlayout = QVBoxLayout()
        vlayout.addWidget(help_button)
        vlayout.addWidget(restart_button)
        vlayout.addWidget(new_button)
        hlayout.addLayout(vlayout)
        self.setLayout(hlayout)
        self.setWindowTitle('Bonsai Ninja')
        self.resize(1200, 600)

    def help_dialog(self):
        QMessageBox.information(None, 'Game controls',
                                'Use simple click to select a node or a branch on the first (left) tree.\n'
                                'Double click on a node to flip the subtree.\n'
                                'After selection, right click on the branch to cut.\n'
                                'After cutting a branch, drag and drop the new subtree root '
                                'to another branch to regraft.\n'
                                'A level is complete when the two trees are identical.',
                                QMessageBox.Ok, QMessageBox.Ok)

    def closeEvent(self, event):
        msg = QMessageBox.question(None, 'Confirm exit', 'Are you sure you want to quit Bonsai Ninja?',
                                   QMessageBox.Yes | QMessageBox.Cancel, QMessageBox.Yes)

        if msg == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


def delay(duration):
    time_stop = QTime.currentTime().addSecs(duration)
    while QTime.currentTime() < time_stop:
        QApplication.processEvents()


def exception_hook(exctype, value, traceback):
    """!
    @brief Needed for suppressing traceback silencing in newer version of PyQt5
    """
    sys._excepthook(exctype, value, traceback)
    sys.exit(1)


if __name__ == '__main__':
    # suppress explicitly traceback silencing
    sys._excepthook = sys.excepthook
    sys.excepthook = exception_hook

    os.chdir(os.getcwd())
    app = QApplication(sys.argv)
    window = HelloWorld()
    window.show()
    app.exec_()


"""
the abstract data structure
"""


class TreeNode:
    """
    Node in a rooted leaf-labeled full binary forest
    """
    def __init__(self, key):
        self.mother = None
        self.left_child = None
        self.right_child = None
        self.key = key
        self.label = str(key)
        self.color = '#555'

    def __repr__(self):
        return self.label

    def add_child(self, child):
        if self.left_child is None:
            self.left_child = child
        else:
            self.right_child = child
        child.mother = self

    def get_sibling(self):
        assert self.mother is not None
        if self == self.mother.left_child:
            return self.mother.right_child
        return self.mother.left_child

    def is_root(self):
        return self.mother is None

    def is_leaf(self):
        return self.left_child is None and self.right_child is None

    def subtree_node_set(self):
        """ all the nodes in the subtree """
        if self.is_leaf():
            return {self}
        subset = self.left_child.subtree_node_set().union(self.right_child.subtree_node_set())
        subset.add(self)
        return subset

    def leaf_list(self):
        """ List of leaves in the subtree """
        if self.is_leaf():
            return [self]
        return self.left_child.leaf_list() + self.right_child.leaf_list()

    def flip(self):
        """ Switch the left and right subtree """
        if self.is_leaf():
            return
        self.left_child, self.right_child = self.right_child, self.left_child

    def cut(self):
        """ Cut the edge between mother and this """
        grandmother = self.mother.mother
        assert grandmother is not None
        if self.mother == grandmother.left_child:
            grandmother.left_child = self.get_sibling()
        else:
            grandmother.right_child = self.get_sibling()
        self.get_sibling().mother = grandmother
        self.mother = None


class Tree:
    """ Abstract class """
    def __init__(self):
        self.root = None
        self.nodes = []
        self.links = []
        self.max_key = 0
        self.size = len(self.nodes)
        self.nb_leaves = (len(self.nodes) + 1) // 2

    def linearize(self):
        self.linearize_(self.root)

    def linearize_(self, node):
        if not node.is_leaf():
            if node.left_child:
                self.linearize_(node.left_child)
            if node.right_child:
                self.linearize_(node.right_child)
        self.nodes.append(node)

    def __iter__(self):
        for u in self.nodes:
            yield u

    def __repr__(self):
        return self.post_order_string(self.root)

    def post_order_string(self, node):
        if node.is_leaf():
            return node.label
        left_string, right_string = '', ''
        if node.left_child:
            left_string = self.post_order_string(node.left_child)
        if node.right_child:
            right_string = self.post_order_string(node.right_child)
        return '(' + ','.join([left_string, right_string]) + ')'


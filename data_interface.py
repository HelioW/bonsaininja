from data_structures import Tree
from graphical_elements import *
from util import *


_OPEN_BRACKET = '('
_CLOSED_BRACKET = ')'
_CHILD_SEPARATOR = ','


def from_newick(newick, label_prefix=''):
    if newick[-1] == ';':
        newick = newick[:-1]
    key, cursor, brackets = 0, 0, 0
    root = Node(key)
    root.label = label_prefix + str(key)
    key += 1
    current_node = root

    while cursor < len(newick):
        character = newick[cursor]
        if character == _OPEN_BRACKET:
            new_node = Node(key)
            new_node.label = label_prefix + str(key)
            current_node.add_child(new_node)
            current_node = new_node
            key += 1
            cursor += 1
            brackets += 1
        elif character == _CHILD_SEPARATOR:
            new_node = Node(key)
            new_node.label = label_prefix + str(key)
            assert current_node.mother.right_child is None, 'too much children'
            current_node.mother.add_child(new_node)
            current_node = new_node
            key += 1
            cursor += 1
        elif character == _CLOSED_BRACKET:
            current_node = current_node.mother
            cursor += 1
            brackets -= 1
            if brackets < 0:
                return None
        else:
            label = [character]
            cursor += 1
            while cursor < len(newick) and newick[cursor] not in (_CHILD_SEPARATOR, _CLOSED_BRACKET):
                label.append(newick[cursor])
                cursor += 1
            label = ''.join(label).strip()
            current_node.label = label
    key += 1
    dummy_root = Node(key)
    dummy_root.label = label_prefix + str(key)
    key += 1
    fantom_leaf = Node(key)
    fantom_leaf.label = 'fantom'
    fantom_leaf.is_fantom = True
    dummy_root.add_child(root).add_child(fantom_leaf)
    return dummy_root


class GraphicalTree(Tree):
    def __init__(self, pair, newick):
        super().__init__()
        self.pair = pair  # pointer to the pair of tree instance
        self.root = from_newick(newick, 'p')
        self.linearize()
        self.nb_leaves = (len(self.nodes) - 1) // 2
        self.compute_coordinates()

    def linearize(self):
        # first time construction
        self.linearize_(self.root)
        for node in self:
            self.init_node(node)

    def linearize_(self, node):
        if node.mother is not None:
            init_link(node.mother, node)
        super().linearize_(node)

    def init_node(self, node):
        node.root = self
        node.pair = self.pair
        if node.is_leaf():
            node.box = Box(node, 20, 20)
        else:
            node.box = Box(node, 10, 10)
        node.box.setParentItem(node)
        self.max_key = max(self.max_key, node.key)

    def newick(self):
        return self.post_order_string(self.root)[1:-8]

    def compute_coordinates(self, x_offset=0):
        leaf_index = 0
        for node in self:
            if node.is_leaf():  # level 0
                node.setX(10 + leaf_index * 50 + x_offset)
                node.setY(10)
                leaf_index += 1
            else:
                leaves = node.leaf_list()
                node.setX(sum([leaf.x() for leaf in leaves if not leaf.is_fantom]) // len(leaves))
                node.setY(max(node.left_child.y(), node.right_child.y()) + 50)
        self.original_root().setY(self.original_root().y()-10)
        self.root.setY(self.root.y()+10)
        self.root.setX(self.original_root().x())
        self.update()

    def original_root(self):
        if self.root.left_child.is_fantom:
            return self.root.right_child
        return self.root.left_child

    def update(self):
        for node in self:
            node.setPos(node.x(), node.y())
        for node in self:
            if node.link:
                node.link.update_polygon()

    def regraft(self, a, b):
        mb = Node(self.max_key+1)  # midpoint of (pb,b)
        mb.label = 'p' + str(mb.key)
        # going from ((x,b)pb) to ((x,(a,b)mb)pb)
        pb = b.mother
        if b == pb.left_child:
            pb.left_child = None
        else:
            pb.right_child = None
        pb.add_child(mb)
        mb.add_child(b).add_child(a)
        self.init_node(mb)


class PairOfTrees:
    def __init__(self, nw1, nw2, color_theme):
        self.tree1 = GraphicalTree(self, nw1)  # reference
        self.tree2 = GraphicalTree(self, nw2)  # tree to manipulate
        assert self.tree1.nb_leaves == self.tree2.nb_leaves
        self.nb_leaves = self.tree1.nb_leaves-1
        self.regraft_mode = False
        self.color_theme = color_theme
        self.label_to_color_value = {}

        # compute coordinates and colors
        self.init_trees_graphics()

    def init_trees_graphics(self):
        self.build_colors()
        self.tree2.compute_coordinates()
        offset = max([n.x() for n in self.tree2.root.leaf_list()]) + 50
        self.tree1.compute_coordinates(offset)
        self.enable_selection()

    def __iter__(self):
        for node in self.tree1:
            yield node
        for node in self.tree2:
            yield node

    def update(self):
        self.tree1 = GraphicalTree(self, self.tree1.newick())
        self.tree2 = GraphicalTree(self, self.tree2.newick())
        self.build_colors()
        self.init_trees_graphics()

    def build_colors(self):
        leaf_colors = {}
        leaf_index = 0
        for leaf in self.tree1.root.leaf_list():
            if leaf.is_fantom:
                continue
            color_index = leaf_index / self.nb_leaves
            leaf_index += 1
            leaf.color = QColor(get_cmap_color(self.color_theme, color_index))
            leaf_colors[leaf.label] = leaf.color
            self.label_to_color_value[leaf.label] = color_index

        for leaf in self.tree2.root.leaf_list():
            if leaf.is_fantom:
                continue
            leaf.color = leaf_colors[leaf.label]

    def change_color_theme(self, color_theme):
        self.color_theme = color_theme
        for node in self:
            if node.is_leaf() and not node.is_fantom:
                value = self.label_to_color_value[node.label]
                node.color = QColor(get_cmap_color(color_theme, value))
                node.update()

    def enable_selection(self):
        for node in self.tree1:
            if not node.is_leaf() or node.is_fantom:
                continue
            node.setFlag(QGraphicsItem.ItemIsSelectable)
            node.disabled = False
        for node in self.tree2:
            if node.mother is None or node.is_fantom:
                continue
            node.setFlag(QGraphicsItem.ItemIsSelectable)
            node.disabled = False
            if node.link and not node.get_sibling().is_fantom:
                # cannot select the edge between the dummy root and the root
                # cannot select the edge to the sibling of a leaf just below the root
                if node.get_sibling().is_leaf() and node.mother == self.tree2.original_root():
                    continue

                node.link.disabled = False
                node.link.setFlag(QGraphicsItem.ItemIsSelectable)
                node.link.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
                node.setFlag(QGraphicsItem.ItemIsMovable)
                node.setFlag(QGraphicsItem.ItemSendsGeometryChanges)

    def enter_regraft_mode(self, new_root):
        self.regraft_mode = True
        # disable all node and edge selection and drag
        for node in self.tree2:
            node.disabled = True
            if node.link:
                node.link.disabled = True
        # enable new subtree drag
        new_root.disabled = False
        for node in new_root.subtree_node_set():
            node.movable = True

    def leave_regraft_mode(self):
        self.regraft_mode = False
        self.enable_selection()
        for node in self.tree2:
            node.movable = False

    def start_dragging(self, node):
        pass

    def end_dragging(self, node, link):
        if not link:
            return
        distance, link = link
        success = distance <= 25.01
        if success:
            self.tree2.regraft(node, link.tail)
            self.update()
            self.leave_regraft_mode()
            return True
        return False

    def level_finished(self):
        return str(self.tree1) == str(self.tree2)


from PyQt5.QtCore import (QLineF, QRectF, QPoint, QPointF, QTime, Qt)
from PyQt5.QtGui import (QPen, QBrush, QColor, QPolygonF, QPainterPath, QTransform)
from PyQt5.QtWidgets import *
from data_structures import TreeNode
import math


class Link(QGraphicsLineItem):
    def __init__(self):
        super().__init__()
        self.head = None
        self.tail = None
        self.selection_offset = 12
        self.selection_polygon = None
        self.disabled = True
        self.highlight_mid = False
        self.is_dangling = False
        self.setZValue(-1)

        self.pen = QPen(Qt.white, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
        self.dashed_pen = QPen(Qt.white, 1, Qt.DashLine)
        self.brush = QBrush(Qt.white)

    def __repr__(self):
        return f'Link to {self.tail}'

    def line(self):
        return QLineF(self.head.x()+self.head.box.boundingRect().width()//2,
                      self.head.y()+self.head.box.boundingRect().height()//2,
                      self.tail.x()+self.tail.box.boundingRect().width()//2,
                      self.tail.y()+self.tail.box.boundingRect().height()//2)

    def midpoint(self):
        return QPointF((self.head.x()+self.head.box.boundingRect().width()//2 +
                        self.tail.x()+self.tail.box.boundingRect().width()//2)//2,
                       (self.head.y()+self.head.box.boundingRect().height()//2 +
                        self.tail.y()+self.tail.box.boundingRect().height()//2)//2)

    def update_polygon(self):
        if self.head is None or self.tail is None:
            return
        angle = self.line().angle() * math.pi / 180
        dx = int(self.selection_offset * math.sin(angle))
        dy = int(self.selection_offset * math.cos(angle))
        offset1 = QPointF(dx, dy)
        offset2 = QPointF(-dx, -dy)
        self.selection_polygon = QPolygonF([self.line().p1()+offset1, self.line().p1()+offset2,
                                            self.line().p2()+offset2, self.line().p2()+offset1])
        self.update()

    def paint(self, painter, options, widget=None):
        if self.tail.is_fantom:
            return
        painter.setPen(self.pen)
        painter.drawLine(self.line())
        if self.isSelected():
            painter.setPen(self.dashed_pen)
            painter.drawPolygon(self.selection_polygon)
        if self.highlight_mid:
            painter.setBrush(Qt.white)
            painter.drawEllipse(self.midpoint(), 10, 10)

    def boundingRect(self):
        return self.selection_polygon.boundingRect()

    def shape(self):
        path = QPainterPath()
        path.addPolygon(self.selection_polygon)
        return path

    def mousePressEvent(self, event):
        if self.disabled:
            event.ignore()
            return
        if self.isSelected() and event.button() == Qt.RightButton:
            self.tail.cut()
        event.accept()


class Box(QGraphicsRectItem):
    def __init__(self, parent, width, height):
        super().__init__(0, 0, width, height)
        self.setParentItem(parent)


class Node(TreeNode, QGraphicsItem):
    def __init__(self, key, label=''):
        TreeNode.__init__(self, key)
        QGraphicsItem.__init__(self)
        self.is_fantom = False

        self.label = label
        self.box = None  # construct the graphical attributes once the entire tree is read
        self.link = None
        self.color = QColor(255, 255, 255, 255)
        self.pen = QPen(Qt.white, 1, Qt.SolidLine)
        self.highlight_pen = QPen(Qt.red, 2, Qt.SolidLine)

        self.root = None  # pointer to the Tree root
        self.pair = None  # pointer to the Pair of trees instance
        self.disabled = True  # selecting with mouse click
        self.movable = False  # movable only when it is cut

    def copy(self):
        n = Node(self.key, self.label)
        n.color = self.color
        n.is_fantom = self.is_fantom
        return n

    def __repr__(self):
        if self.is_leaf():
            if self.mother:
                return 'P'+self.mother.label+':'+self.label
            return self.label
        return self.label + '(' + self.left_child.label + ', ' + self.right_child.label + ')'

    def __str__(self):
        return self.label

    def add_child(self, node):
        super().add_child(node)
        link = Link()
        link.tail = node
        link.head = self
        node.link = link
        return self

    def boundingRect(self):
        pos = self.box.rect().topLeft()
        return QRectF(pos.x(), pos.y(),
                      self.box.boundingRect().width()+10,
                      self.box.boundingRect().height()+10)

    def paint(self, painter, options, widget=None):
        if self.is_fantom:
            return
        painter.setPen(self.pen)
        painter.setBrush(self.color)
        painter.drawEllipse(self.box.rect())
        if self.isSelected():
            painter.setPen(self.highlight_pen)
            if not self.is_leaf():
                painter.setBrush(Qt.red)
            painter.drawEllipse(self.box.rect())

    def mousePressEvent(self, event):
        if self.disabled:
            event.ignore()
            return
        self.setCursor(Qt.ClosedHandCursor)
        super().mousePressEvent(event)

    def mouseDoubleClickEvent(self, event):
        if self.disabled:
            event.ignore()
            return
        self.flip()
        event.accept()

    def mouseMoveEvent(self, event):
        if not self.movable:
            event.ignore()
            return
        super().mouseMoveEvent(event)

    def update(self):
        super().update()
        if not self.is_leaf():
            self.left_child.link.update()
            self.left_child.link.update()

    def flip(self):
        if self.pair.regraft_mode:
            return
        super().flip()
        self.pair.update()
        self.scene().flipped()

    def cut(self):
        self.link.setVisible(False)
        self.scene().increment_moves()
        if self.mother.mother is not None:
            self.mother.link.is_dangling = True
        self.link.is_dangling = True
        self.get_sibling().link.is_dangling = True
        super().cut()
        self.pair.enter_regraft_mode(self)


def init_link(mother, node):
    link = Link()
    link.head = mother
    link.tail = node
    node.link = link


from data_interface import PairOfTrees
import random
import os
import sys
bundle_dir = getattr(sys, '_MEIPASS', os.path.abspath(os.path.dirname(__file__)))
path_to_data = os.path.abspath(os.path.join(bundle_dir, 'data.txt'))


class DataSpawner:
    def __init__(self):
        self.names = []
        self.instances = {}
        with open(path_to_data, 'r') as input_file:
            while True:
                line = input_file.readline()
                if not line:
                    break
                name, distance = line.split()
                nw1 = input_file.readline().rstrip()
                nw2 = input_file.readline().rstrip()
                self.instances[name] = (distance, nw1, nw2)
                if name[:6] != 'simple':
                    self.names.append(name)

    def spawn(self, color_theme, previous_level, restart):
        if not restart:
            # only 3 simple levels
            if previous_level[:6] != 'simple' or previous_level == 'simple3':
                # random level that is different than the previous one
                new_level = random.choice([level for level in self.names if level != previous_level])
            else:
                simple_index = int(previous_level[6])
                new_level = f'simple{simple_index+1}'
        else:
            new_level = previous_level
        distance, nw1, nw2 = self.instances[new_level]
        return new_level, distance, PairOfTrees(nw1, nw2, color_theme)




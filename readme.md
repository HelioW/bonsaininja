## Releases

https://helio-wang.itch.io/bonsai-ninja


## Build

First you need to have Python3.6 (https://www.python.org/downloads/release/python-368/).

Install the workflow manager Pipenv:
```
pip install --user pipenv
```

Clone the project and install the dependencies (it takes some time):
```
git clone https://bitbucket.org/HelioW/bonsaininja.git
cd bonsaininja
pipenv install
```

You can test it in Python:
```
pipenv run python main.py
```

Create the executable:

```
pipenv run python -m PyInstaller main.py -F
dist/main.exe
```
